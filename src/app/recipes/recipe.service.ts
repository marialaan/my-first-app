import {EventEmitter, Injectable} from '@angular/core';

import { Recipe } from './recipe.model';
import { Ingredient } from "../shared/ingredient.model";
import { ShoppingListService } from "../shopping-list/shopping-list.service";
import { Subject } from "rxjs/Subject";

@Injectable()
export class RecipeService {
    recipesChanged = new Subject<Recipe[]>();

    private recipes: Recipe[] = [
        new Recipe(
            'Penne with courgette flowers',
            'At last - a recipe using courgette flowers without deep-frying! This dish is as colourful as it is easy.',
            'http://ichef.bbci.co.uk/food/ic/food_16x9_448/recipes/smallpennepastawithc_11691_16x9.jpg',
            [
                new Ingredient ('garlic cloves', 2),
                new Ingredient ('anchovy fillets', 2),
                new Ingredient ('small onions, sliced finely', 2),
                new Ingredient ('small courgettes, sliced lengthways finely', 2)
            ]),
        new Recipe(
            'Chargrilled broccoli pasta with chilli and garlic',
            'This quick and easy pasta with broccoli is perfect when youve got zero time to cook. Add any extras you like!',
            'http://ichef.bbci.co.uk/food/ic/food_16x9_608/recipes/chargrilled_broccoli_69096_16x9.jpg',
            [
                new Ingredient ('tbsp olive oil', 3),
                new Ingredient ('garlic clove', 1),
                new Ingredient ('red chilli, finely sliced', 1),
                new Ingredient ('tbsp flaked almonds', 1),
                new Ingredient ('tablespoon chili powder', 1),
                new Ingredient ('spaghetti', 1),
            ])
    ];

    constructor(private slService: ShoppingListService) {}

    setRecipes(recipes: Recipe[]) {
        this.recipes = recipes;
        this.recipesChanged.next(this.recipes.slice());
    }

    getRecipes() {
        return this.recipes.slice();
    }

    getRecipe(index: number) {
        return this.recipes[index];
    }

    addIngredientsToShoppingList(ingredients: Ingredient[]) {
        this.slService.addIngredients(ingredients);
    }

    addRecipe(recipe: Recipe) {
        this.recipes.push(recipe);
        this.recipesChanged.next(this.recipes.slice());
    }

    updateRecipe(index: number, newRecipe: Recipe) {
        this.recipes[index] = newRecipe;
        this.recipesChanged.next(this.recipes.slice());
    }

    deleteRecipe(index: number) {
        this.recipes.splice(index, 1);
        this.recipesChanged.next(this.recipes.slice());
    }

}